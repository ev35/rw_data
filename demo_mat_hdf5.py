def read_mat(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    d = loadmat(infile)

    return mat_dict

def read_hdf5(infile):
    """read HDF5 input

    :param infile: input file (str)
    :return: mat_file_ref
    """
    import h5py

    f = h5py.File(infile)

    return f

def read_robust(infile):
    """ read MATLAB or HDF5 input
    """

    try:
        f = read_mat(infile)
        return f
    except:
        hdf5_file = read_hdf5(infile)
        f = dict(hdf5_file)
        return f

if __name__ == "__main__":
    read_robust('mat_v5.mat')
    read_robust('mat_v73.mat')